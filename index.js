// console.log('Hello World!')

/*
	fetch() is a method in JS, which allows to send request to an API and process its response.fetch has 2 arguments, the url to resource/route and optional object which contains additional information about our request such as method, the body, and headers of request.

	fetch() method
	Syntax: 
		fetch(url, options)

*/

// Get post data using fetch() - does not require additional option/s; Get request is the only exception

let posts = []
let count = 1

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((data) => console.log(data))


// Add POst Data
document.querySelector('#form-add-post').addEventListener('submit', (e) => {

	e.preventDefault();
// Start of fetch
	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1,
		}),
		headers: {
			'Content-type': 'application/json; charset=UTF-8'
		}

	})
	.then((response)=> response.json())
	.then((data) => {
	console.log(data)
	alert("Successfully Added.")

	// allows us to clear text elements upon post creation
	document.querySelector('#txt-title').value = null;
	document.querySelector('#txt-body').value = null;
	})
// End of Request

})

// Show Post

const showPosts = (posts) => {
	let postEntries = ''

	posts.forEach((post) => {

		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})

	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

// Retrieve All Posts
/*fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((data) => {
	showPosts(data)
})*/


// Edit post
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id
	document.querySelector('#txt-edit-title').value = title
	document.querySelector('#txt-edit-body').value = body

	// use removeAttribute methose to remove disabled attr from the button
	document.querySelector('#btn-submit-update').removeAttribute('disabled')

}

// Update Post 

document.querySelector('#form-edit-post').addEventListener('submit', (e)=> {

	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: "PUT",
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
		}),
		headers: {'Content-type': 'application/json; charset=UTF-8'}
	})
	.then((response)=> response.json())
	.then((data)=>{
		console.log(data)
		alert('Successfully updated')

		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;

		// Re-enables the attribute previously disabled
		document.querySelector('#btn-submit-update').setAttribute('disabled', true)
	})
})

// Retrieve Single Post
fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response) => response.json())
.then((data) => console.log(data))


fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((data) => {
	showPosts(data)
})

const deletePost = (id) => {
	
	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: "DELETE"
	})
	.then((response) => response.json())
	.then((data) =>{ 
	document.querySelector(`#post-${id}`).remove()
	})
	
	alert("Successfully deleted")
}

